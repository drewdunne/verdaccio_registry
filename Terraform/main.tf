﻿terraform {
    required_version = ">= 0.12"
    backend "s3" {
        bucket = "verdaccio-registry"
        key = "us-west-2/state.tfstate"
        region = "us-west-2"
        profile = "drewdunne"
    }
}

provider "aws" {
  access_key = var.drewdunne_aws_public
  secret_key = var.drewdunne_aws_secret
  region = "us-west-2"
}

locals {
  name = "verdaccio"
}

resource "aws_ecr_repository" "this" {
  name = local.name
}

resource "aws_ecs_cluster" "this" {
  name = local.name
}

resource "aws_security_group" "this" {
  name = local.name
}

resource "aws_security_group_rule" "this" {
  security_group_id = aws_security_group.this.id

  type        = "ingress"
  from_port   = 4873
  to_port     = 4873
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_ecs_task_definition" "this" {
  family                   = local.name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn

  container_definitions = jsonencode([
    {
      name      = local.name
      image     = "${aws_ecr_repository.this.repository_url}:latest"
      essential = true
      portMappings = [
        {
          containerPort = 4873
          hostPort      = 4873
        }
      ]
    }
  ])
}

resource "aws_iam_role" "ecs_execution_role" {
  name               = "ecsTaskExecutionRoleVerdaccio"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ecs_execution_role" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs_execution_role.name
}

resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = local.name
  }
}

resource "aws_subnet" "this" {
  count = 2

  cidr_block = "10.0.${count.index + 1}.0/24"
  vpc_id     = aws_vpc.this.id

  tags = {
    Name = "${local.name}-${count.index + 1}"
  }
}

resource "aws_security_group" "ecs_security_group" {
  name = "ecs_security_group"
  vpc_id = aws_vpc.this.id
}

resource "aws_security_group_rule" "ecs_security_group_rule" {
  security_group_id = aws_security_group.ecs_security_group.id

  type        = "ingress"
  from_port   = 4873
  to_port     = 4873
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id
}

resource "aws_route_table" "this" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = {
    Name = local.name
  }
}

resource "aws_route_table_association" "this" {
  count = 2

  subnet_id      = aws_subnet.this[count.index].id
  route_table_id = aws_route_table.this.id
}

resource "aws_network_acl" "this" {
  vpc_id = aws_vpc.this.id

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 4873
    to_port    = 4873
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  }

  egress {
    protocol   = "-1"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = local.name
  }
}

resource "aws_network_acl_association" "this" {
  count = 2

  subnet_id       = aws_subnet.this[count.index].id
  network_acl_id  = aws_network_acl.this.id
}